namespace ClassLibrary1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialised : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Catagories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Name = c.String(),
                        Description = c.String(),
                        catagory_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Catagories", t => t.catagory_ID)
                .Index(t => t.catagory_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "catagory_ID", "dbo.Catagories");
            DropIndex("dbo.Products", new[] { "catagory_ID" });
            DropTable("dbo.Products");
            DropTable("dbo.Catagories");
        }
    }
}

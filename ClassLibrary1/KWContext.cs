﻿using kiwiWearEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
 public   class KWContext : DbContext
    {
        public KWContext():base("KiwiWearConnection")
        {

        }

        public DbSet<Product>products { get; set; }
        public DbSet<Catagory> catagories { get; set; }

    }
}

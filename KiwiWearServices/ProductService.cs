﻿using ClassLibrary1;
using kiwiWearEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KiwiWearServices
{
 public   class ProductService
    {
        public Product GetProduct(int id)
        {
            using (var context = new KWContext())
            {
                return context.products.Find(id);
            }
        }

        public List<Product> GetProducts()
        {
            using (var context = new KWContext())
            {
                return context.products.ToList();
            }
        }

        public void SaveProduct(Product product)
        {
            using (var context = new KWContext())
            {
                context.products.Add(product);
                context.SaveChanges();
            }
        }
        public void updateProduct(Product product)
        {
            using (var context = new KWContext())
            {
                context.Entry(product).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }
        public void DeleteProduct(int ID)
        {
            using (var context = new KWContext())
            {
                var product = context.products.Find(ID);
                // context.Entry(catagory).State = System.Data.Entity.EntityState.Deleted;
                context.products.Remove(product);
                context.SaveChanges();
            }
        }
    }
}

﻿using ClassLibrary1;
using kiwiWearEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KiwiWearServices
{
 public   class CatagoryService
    {
        public Catagory GetCatagory(int id)
        {
            using (var context = new KWContext())
            {
                return context.catagories.Find(id);
            }
        }

        public List<Catagory> GetCatagories()
        {
            using (var context = new KWContext())
            {
                return context.catagories.ToList();
            }
        }

        public void SaveCatagory(Catagory catagory)
        {
            using (var context = new KWContext())
            {
                context.catagories.Add(catagory);
                context.SaveChanges();
            }
        }
        public void updateCatagory(Catagory catagory)
        {
            using (var context = new KWContext())
            {
                context.Entry(catagory).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }
        public void DeleteCatagory(int ID)
        {
            using (var context = new KWContext())
            {
                var catagory = context.catagories.Find(ID);
                // context.Entry(catagory).State = System.Data.Entity.EntityState.Deleted;
                context.catagories.Remove(catagory);
                context.SaveChanges();
            }
        }
    }
}

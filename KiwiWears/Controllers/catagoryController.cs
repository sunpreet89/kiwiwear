﻿using kiwiWearEntities;
using KiwiWearServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KiwiWears.Controllers
{
    public class catagoryController : Controller
    {
        CatagoryService catagoryService = new CatagoryService();
        [HttpGet]
        public ActionResult Index()
        {
            var catagories = catagoryService.GetCatagories();
            return View(catagories);
        }


        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Catagory catagory)
        {
            catagoryService.SaveCatagory(catagory);
            return View();
        }

        [HttpGet]
        public ActionResult Edit(int ID)
        {

            var catagory = catagoryService.GetCatagory(ID);
            return View(catagory);
        }
        [HttpPost]
        public ActionResult Edit(Catagory catagory)
        {
            catagoryService.updateCatagory(catagory);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int ID)
        {

            var catagory = catagoryService.GetCatagory(ID);
            return View(catagory);
        }
        [HttpPost]
        public ActionResult Delete(Catagory catagory)
        {
            catagory = catagoryService.GetCatagory(catagory.ID);

            catagoryService.DeleteCatagory(catagory.ID);

            
            return RedirectToAction("Index");
        }
    }
}